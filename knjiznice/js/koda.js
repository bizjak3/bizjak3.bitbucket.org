
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var ehrIdPacient1 = "b8a172a3-72e6-4553-97e6-8773ba3ab6e0";
var ehrIdPacient2 = "1d4164b3-7ba2-4ccd-9e37-4993e579c70b";
var ehrIdPacient3 = "18f48352-a56e-46ad-abc9-b7d8f8fb568f";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 

window.onload = function () {
    
    document.querySelector("#generiraj").addEventListener('click', generirajVse);
    
    jQuery("#pacient4").click(function(e){
        var ehrIdPacient4 = document.querySelector("#ehrIdVpisanega").value;
        pridobiPodatke(ehrIdPacient4,function(pacient){
            //console.log(pacient);
            sprocesiraj(pacient);
        })
    });
    
    jQuery("#pacient1").click(function(e){
        pridobiPodatke(ehrIdPacient1,function(pacient){
            //console.log(pacient);
            sprocesiraj(pacient);
        })
    });
    
    jQuery("#pacient2").click(function(e){
        pridobiPodatke(ehrIdPacient2,function(pacient){
            //console.log(pacient);
            sprocesiraj(pacient);
        })
    });

    jQuery("#pacient3").click(function(e){
        pridobiPodatke(ehrIdPacient3,function(pacient){
            //console.log(pacient);
            sprocesiraj(pacient);
        })
    });

    //document.querySelector("#pacient3").addEventListener('click', pridobiPodatke(ehrIdPacient3,function(pacient){
     //   console.log(pacient);
   // }));
};


/*function BMIizracun(weight, height, gender){
    
  
  
  $.ajaxSetup({
     headers:{"X-Mashape-Key":"KudWqUTY1RmshJSS8ESVUJWLDKwep1cdtEujsn2d7EKboSEFSo"}
  });
  
		$.ajax({
		    url: "https://bmi.p.mashape.com/",
		    type: 'POST',
		    success: function (data) {
		        console.log(data);
		    }
		    });
}*/

function 
sprocesiraj(pacient){

   var ocena =0;
   var ocena1=3;
  
    $("#tabela").removeClass('hidden');
    $("#ime").html(pacient[0]+" "+pacient[1]);
    $("#pritisk").html(pacient[8]+"/"+pacient[7]);
    
    if(pacient[7]<90&&pacient[8]<60)
        $("#krvniRez").html("<b>Imate nizek krvni pritisk!</b>");
    else if(pacient[7]>120&&pacient[8]>80)
        $("#krvniRez").html("<b>Imate visok krvni pritisk!</b>");
    else{
        $("#krvniRez").html("Vaš krvni tlak je normalen");
        ocena++;
        ocena1--;
    }
        
    var BMI = pacient[5]/((pacient[4]/100)*(pacient[4]/100));
        
    $("#BMI").html(BMI);
    
    if(BMI>24.9)
        $("#BMIRez").html("<b>Vaš BMI je previsok!</b>");
    else if(BMI<18.5)
        $("#BMIRez").html("<b>Vaš BMI je prnizek!</b>");
    else{
         $("#BMIRez").html("Vaš BMI je ustrezen");
         ocena++;
         ocena1--;
    }
         
    $("#temp").html(pacient[6]);
    
    if(pacient[6]>37){
        $("#tempRez").html("<b>Imate povišano telesno temperaturo!</b>");
        ocena = 0;
        ocena1 = 1;
    }
    else if(pacient[6]<36){
        $("#tempRez").html("<b>Imate prenizko telesno temperaturo!</b>");
        ocena = 0;   
        ocena1 = 1;
    }
    else{
        $("#tempRez").html("Vaša telesna temperatura je normalna");        
        ocena++;
        ocena1--;
    }
    if(ocena==3)
        $("#ocena").html("<b>Vaše zdravje je odlično!</b>");
    if(ocena==2)
        $("#ocena").html("<b>Vaše zdravje je dobro</b>");
    if(ocena==1)
        $("#ocena").html("<b>Vaše zdravje je slabo!</b>");
    if(ocena==0)
        $("#ocena").html("<b>Vaše zdravje je zelo slabo!</b>");
        
    $('#myChart').remove();
    $('#graph-container').append('<canvas id="myChart" width="200" height="200"><canvas>');
    
    
    var ctx = document.getElementById("myChart").getContext('2d');
    ctx.delete;
    var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ["Ne zdrav delež", "Zdrav Delež"],
        datasets: [{
            data: [ocena1,ocena],
            backgroundColor: [
                'rgba(255, 0, 0, 0.2)',
                'rgba(0, 255, 0, 0.2)'
            ],
            borderColor: [
                'rgba(255,0,0,1)',
                'rgba(0, 255, 0, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero:true
            }
        }]
    }
    }
    
});
    
      $.ajax({
        type: "GET",
        url: "https://sl.wikipedia.org/w/api.php?action=parse&format=json&prop=text&selection=0&page=zdravje&callback=?",
        contentType: "application/json; charset=utf-8",
        async: false,
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            
            var markup = data.parse.text["*"];
            var izpis = $('<div></div>').html(markup);
            $('#wiki').html($(izpis).find('p'));
            //console.log(izpis);
        },
        error: function (errorMessage) {
        }
    });
    
}


function generirajVse(){
    pacient1=[
        "John",
        "Pavel",
        "MALE",
        "1975-05-20",
        "2000-01-01T10:00Z",
        "190",
        "110",
        "37.5",
        "130",
        "90",
        "98",
        "medicinska sestra Franja"
        ];
    generirajPodatke(pacient1, function(id1){
        ehrIdPacient1 = id1;
        //console.log(id1);
        //console.log(ehrIdPacient1);
        dodajMeritveVitalnihZnakov(ehrIdPacient1,pacient1);
        //console.log("ID1 shranjen "+ehrIdPacient1);
    });
    
    pacient2=[
        "Maria",
        "Benedikt",
        "FEMALE",
        "1950-01-10",
        "2000-02-01T11:00Z",
        "160",
        "100",
        "36.5",
        "110",
        "80",
        "80",
        "medicinska sestra Nives"
        ];
    generirajPodatke(pacient2, function(id2){
        ehrIdPacient2 = id2;
        dodajMeritveVitalnihZnakov(ehrIdPacient2,pacient2);
        console.log("ID2 shranjen "+ehrIdPacient2);
    });
    
    pacient3=[
        "Linus",
        "Sebastian",
        "MALE",
        "1990-01-10",
        "2000-03-01T11:00Z",
        "185",
        "70",
        "36.6",
        "110",
        "70",
        "95",
        "medicinska sestra Nives"
        ];
    generirajPodatke(pacient3, function(id3){
        ehrIdPacient3 = id3;
        dodajMeritveVitalnihZnakov(ehrIdPacient3,pacient3);
        console.log("ID3 shranjen "+ehrIdPacient3);
    });
    $("#tabela").addClass('hidden');
    window.alert("Generiranje je uspelo!\n"+"EHRid1 = "+ ehrIdPacient1+"\n"+"EHRid2 = "+ehrIdPacient2+"\n"+"EHRid3 = "+ehrIdPacient3+"\nVrednosti so avtomatsko vnešene pod pacient1, pacient2 in pacient3 in jih lahko izberete iz dropdown menija.");
}



function generirajPodatke(pacient,callback) {
  var ehrId = "";
  
  var pacient1=pacient;
  $.ajaxSetup({
      headers:{"Ehr-Session":getSessionId()}
  });
  
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: pacient1[0],
		            lastNames: pacient1[1],
		            gender: pacient1[2],
		            dateOfBirth: pacient1[3],
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                   //console.log("Generiranje je uspelo: "+ehrId);
		                   callback(ehrId);
		                }
		            },
		            error: function(err) {
		            	console.log("Napaka:"+err);
		            }
		        });
		    }
		});
  return ehrId;
}

function pridobiPodatke(id,callback){
    var ehrId = id;
    var pacient=[
        "Ime",
        "Priimek",
        "Spol",
        "datumRojstva",
        "Visina",
        "Teza",
        "Temp",
        "Sisto",
        "Diasto",
        "Nasicenost",
        ];
        
        
        $.when(
                $.ajaxSetup({
                  headers:{"Ehr-Session":getSessionId()}
                }),
                
                $.ajax({
            			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            			type: 'GET',
            	    	success: function (data) {
            	    	    var party = data.party;
            	    	    pacient[0] = party.firstNames;
            	    	    pacient[1] = party.lastNames;
            	    	    pacient[2] = party.gender;
            	    	    pacient[3] = party.dateOfBirth;
            	    	},
            	    	 error: function(err) {
            	    	        window.alert("Izbrani pacient ne obstaja! Prosimo preverite ehrId ali pritisnite na gumb Generiranje podatkov");
            	    	        $("#tabela").addClass('hidden');
                				console.log("Napaka:"+err);
                				
                			}
                		}),
	    	    $.ajax({
            	    url: baseUrl+"/view/"+ehrId+"/height",
            	    type: 'GET',
            	    success: function(data){
            	        pacient[4] = data[data.length-1].height;
            	    },
            	    error: function(err){
            	        console.log("Napaka: "+err);
            	    }
            	}),
            	$.ajax({
            	    url: baseUrl+"/view/"+ehrId+"/weight",
            	    type: 'GET',
            	    success: function(data){
            	        pacient[5] = data[data.length-1].weight;
            	    },
            	    error: function(err){
            	        console.log("Napaka: "+err);
            	    }
            	}),
            	$.ajax({
            	    url: baseUrl+"/view/"+ehrId+"/body_temperature",
            	    type: 'GET',
            	    success: function(data){
            	        pacient[6] = data[data.length-1].temperature;
            	    },
            	    error: function(err){
            	        console.log("Napaka: "+err);
            	    }
            	}),
            	$.ajax({
            	    url: baseUrl+"/view/"+ehrId+"/blood_pressure",
            	    type: 'GET',
            	    success: function(data){
            	        pacient[7] = data[data.length-1].systolic;
            	        pacient[8] = data[data.length-1].diastolic;
            	        
            	    },
            	    error: function(err){
            	        console.log("Napaka: "+err);
            	    }
            	}),
            	$.ajax({
            	    url: baseUrl+"/view/"+ehrId+"/spO2",
            	    type: 'GET',
            	    success: function(data){
            	        pacient[9] = data[data.length-1].spO2;
            	        
            	    },
            	    error: function(err){
            	        console.log("Napaka: "+err);
            	    }
            	})
            	).then(function(){
            	    callback(pacient);
            	})
	    	}

function dodajMeritveVitalnihZnakov(id,pacient) {
	sessionId = getSessionId();
	var ehrId = id;

	var ehrId = id;
	var datumInUra = pacient[4];
	var telesnaVisina = pacient[5];
	var telesnaTeza = pacient[6];
	var telesnaTemperatura = pacient[7];
	var sistolicniKrvniTlak = pacient[8];
	var diastolicniKrvniTlak = pacient[9];
	var nasicenostKrviSKisikom = pacient[10];
	var merilec = pacient[11];

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        //console.log("Dodane meritve");
		    },
		    error: function(err) {
		    	console.log("Napaka: "+err);
		    }
		});
}



